<?php 
require "core.php";
include "inc/header.php"; ?>

	<title> Socl. </title>

<?php include 'inc/toolbar.php'; ?>


<?php
	if(!empty($_POST['username'])){
		global $db;
		$username = $_POST['username'];
		$password = $_POST['password'];
		login($db, $username, $password); 
	}

	if(!empty($_POST['file']))

?>
<?php if(logged_in()): 

	$user = get_user($db, $_SESSION['user_id'])


?>


<main>
	<div id='content-flow'>
		<div class='side-pane'>
			<div id='bio-wrapper'>
				<div class='inner-wrapper'>
					<aside id='bio'>
					
						<?php $anon = empty($user['avatar']); ?>		
						<div id='genero-avatar' <?php if($anon){ echo "class='anon'";} ?> >
								<a id='form-toggle' unselectable="on"> Change Avatar </a>
								<img src='<?php echo $user["avatar"]; ?>' id='user-avatar'>
						</div>

							<h4> Hi there, <span class='bold'> <?php echo $user['first_name']; ?> </span> </h4>
					</aside>


					<div id='form-wrapper' class='form-hidden'>
						<div id='avatar-upload-wrapper'>
							<form action="upload.php" method="POST" enctype="multipart/form-data">
								
								<label for='filepicker'> Choose an image </label>
								<input type='file' name='file' id='filepicker'>

								<input type='submit' name='submit' value='Upload'>

							</form>
						</div>
					</div>

					<a href='./logout.php' id='logout'> Logout </a> 
				</div>
			</div>
		</div>
		<div class='main-pane'>


			<header id='user-header'>
				<?php if(!empty($user['header'])) : ?>
					<img src="<?php echo $user['header']; ?>" id='hero-image'>
				<?php endif; ?>
				<div id='hero-form-wrapper'>
					<form id='hero-form' action="hero.php" method="POST" enctype="multipart/form-data">
							<input type='file' name='file' id='filepicker'>
							<input type='submit' name='submit' value='Upload'>
					</form>
				</div>
			</header>
			<div id='post-flow'>
				<div id='create-post'>
					<form id='post-creation' method='post' action='post.php'>
							<textarea id='post-content' name='content' required tabindex="1" placeholder='Say something...'></textarea>
							<input type='submit' tabindex="2" value='post'>
					</form>
				</div>
				<div id='posts-target'></div>
				<?php display_posts($db); ?>

			</div> <!-- Post flow -->

		</div> <!-- Main Pane -->
		
		<div class='clear'></div>

	</div>
</main>

<button id='bday-activator'> Bday </button>

<div id='bday'></div>


<?php else: ?>
<div id='login-wrapper'>
	<main> 
		<div id='login-frame'>
			<div class='inner-wrapper'>
				<div id='form-wrapper'>
					<h2> Login to Socl. </h2>
					<span>
						Sign in or <a href='./register.php'> register </a> and join the Socl community. 

					</span>
					<form id='login-form' method="POST">
						<label for="username"> Username </label>
						<input type='text' name='username' placeholder='Username' id='username' required> 

						<label for='password'> Password </label>
						<input type="password" name='password' placeholder="Password" id='paassword' required>

						<input type='submit' value='Login'>
					</form>
				</div>
			</div>
		</div>

		<div id='login-canvas'>
			<div class='inner-wrapper'>
				<div id='splash-text'>
					<h3> <strike> Myspace.</strike> <strike>Facebook.</strike> Socl. </h3> 
				</div>
			</div>
		</div>

	</main>
</div>


<?php endif; ?>

<script>

document.addEventListener("DOMContentLoaded", function(event) {
	var toggle 	= document.getElementById('form-toggle'),
		form 	= document.getElementById('form-wrapper');

		toggle.addEventListener('click', function(){ 
			form.classList.toggle('form-revealed');
		});
});

$(document).on('submit', '#post-creation', function(ev){
			ev.preventDefault();

			var content = $('#post-content').val();

			jQuery.ajax({
			    type:"POST",
			    url: 'post.php',
			    data: {
			    'content': content
				},
			    success:function(results){
			    	$('#posts-target').append(results);
			    } 
		   	});
});

$(document).on('click', '#bday-activator', function(){
	$('#bday').load('bday.php');
});

</script>

</body>
</html>