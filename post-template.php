 	<?php if(!isset($post)){
 		$post['post_text'] = $content;
 		$post['post_date'] = date('H:i:s', time()); 
 		$post['author_id'] = $_SESSION['user_id'];
 	} ?>

 	<article class='post'>
 		<?php $author = get_user($db, $post['author_id']); ?>
 		<div class='author-wrapper'>
 			<?php if(!empty($author['avatar'])): ?>
 				<img src='<?php echo $author["avatar"]; ?>' />
 			<?php else: ?>
 				<div class='genero-avatar'></div>
 			<?php endif; ?>
 			
 		</div>
 		<div class='post-body'>
 			<strong class='author-name'>
 				<?php echo $author['first_name'] . " " . $author['last_name']; ?>
 			</strong>
 			<p> <?php echo $post['post_text']; ?> </p>
	 		<span class='post-date'> <?php echo $post['post_date']; ?> </span>
 		</div>
 	</article>