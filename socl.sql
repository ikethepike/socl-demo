-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 04, 2015 at 09:51 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `socl`
--

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `post_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `post_text` text NOT NULL,
  `post_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`post_id`, `author_id`, `post_text`, `post_date`) VALUES
(43, 4, 'Who needs facebook?', '2015-11-03 18:35:27'),
(45, 5, 'Hej Svenskar bara ni kan lÃ¤sa det hÃ¤r hemliga meddelandet.', '2015-11-03 22:25:42');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `username` varchar(20) CHARACTER SET latin1 NOT NULL,
  `password` varchar(75) CHARACTER SET latin1 NOT NULL,
  `first_name` text CHARACTER SET latin1 NOT NULL,
  `last_name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `email` varchar(30) CHARACTER SET latin1 NOT NULL,
  `avatar` varchar(50) CHARACTER SET latin1 NOT NULL,
  `header` varchar(50) CHARACTER SET latin1 NOT NULL,
  `priv` int(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `first_name`, `last_name`, `email`, `avatar`, `header`, `priv`) VALUES
(4, 'test ', '$2y$11$md3/Aiq./1YeGzXERwpisO5jtEwGPMSZbgn1iSW985JlZBwOJnfKy', 'test', 'test', 'test@test.com', './uploads/IMG_9366.jpg', '', 0),
(5, 'ikethepike ', '$2y$11$7T62r7v3JkcxgIEEr5NVOeLoNK0s4KgxCsOqdcAcgU38WQs2FEw4S', 'Isaac', 'Kuehnle-Nelson', 'ikethepike@outlook.com', './uploads/14780005 (3).JPG', './uploads/WP_20150730_16_57_33_Pro.jpg', 0),
(6, 'plop ', '$2y$11$nTkTSSGRkdapoFVk0JGHLue.ZKvsvRqtzbhYoAazwnEnuVcH1biAm', 'Ernst', 'Hälberdinger', 'fake@fake.com', '', '', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
