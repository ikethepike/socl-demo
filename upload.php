<?php

require 'core.php';



$uploads_dir = "./uploads/";

$target_file = $uploads_dir . basename($_FILES["file"]["name"]);

$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["file"]["tmp_name"]);
   
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        
        $tmp_name = $_FILES["file"]["tmp_name"];
        $name = $_FILES["file"]["name"];

        if (move_uploaded_file($tmp_name, "$uploads_dir/$name" )) {
			
			global $db; 

			$new_file = (string)$uploads_dir . $name;
		    
		    update_user($db, $_SESSION['user_id'] , 'avatar', $new_file );

		    header("Location: ". BASE_URL );
		    die();

		} else {
		   echo "Upload failed";
		   return false; 
		}

    } else {
        echo "File is not an image.";
        return false;
    }
}
?> 