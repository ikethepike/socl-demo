<?php 

define('BASE_URL', 'http://localhost/socl');

// Database vars
session_start();

$db_user = "root";
$db_pass = ""; 
try{
	global $db;
	$db = new PDO('mysql:host=localhost;dbname=socl', $db_user, $db_pass);

} catch (PDOexception $e) {
	echo $e->getMessage() . "<br/>";
	die();
}

function get_user(PDO $db, $user_query){

	if(is_numeric($user_query)){
		$stmt = $db->prepare('SELECT * FROM `users` WHERE `id` = ?');
	} else {
		$stmt = $db->prepare('SELECT * FROM `users` WHERE `username` = ?');
	}

	$stmt->bindParam(1, $user_query);
	$stmt->execute();	
	$user = $stmt->fetch(); 
	return $user; 
}

function logged_in(){
	if(isset($_SESSION['username']) ){
		return true;
	} else {
		return;
	}
}

function login(PDO $db, $username, $password) {

	$stmt = $db->prepare("SELECT * FROM `users` WHERE `username`= ? LIMIT 1"); 
	$stmt->bindParam(1, $username);
	$stmt->execute(); 
	$user = $stmt->fetch();

	if($user !== false) {
		if(password_verify ( $password , $user['password'] )){
			$_SESSION['username'] = $username;
			$_SESSION['user_id'] = $user['id'];
			return true;
		} else {
			echo "<div class='message error'> Incorrect password. </div>";
		}

	} else {
		echo "<div class='message error'> The user {$username} does not exist. </div>";
	}

}


function get_user_id(PDO $db, $username) {
	$stmt = $db->prepare("SELECT id FROM users WHERE username={$username} LIMIT 1"); 
	$stmt->execute(); 
	$row = $stmt->fetch();

	return $row;
}


function update_user(PDO $db, $user, $field, $value){
	$stmt = $db->prepare("UPDATE users SET `{$field}` = '{$value}' WHERE id= {$user}");

	$status = $stmt->execute();
	if($status){
		echo "User updated successfully.";
		return true;
	} else {
		print_r($stmt);
		echo "Failed to update user";
		return false; 
	}
}


function author_posts(PDO $db, $author_id = null){

	if(!isset($author_id)){
		$author_id = $_SESSION['user_id'];
	}

	$sql = "SELECT * FROM `posts` WHERE `author_id` IN({$author_id}) ORDER BY `post_id` DESC ";
	$posts = $db->query($sql);

	if($posts !== false) {
   
	 foreach($posts as $post) { ?>

	 	<article class='post'>
	 		<?php $author = get_user($db, $post['author_id']); ?>
	 		<div class='author-wrapper'>
	 			<?php if(!empty($author['avatar'])): ?>
	 				<img src='img/uploads/<?php $author["avatar"]; ?>' />
	 			<?php else: ?>
	 				<div class='genero-avatar'></div>
	 			<?php endif; ?>
	 			
	 		</div>
	 		<div class='post-body'>
	 			<strong class='author-name'>
	 				<?php echo $author['first_name'] . " " . $author['last_name']; ?>
	 			</strong>
	 			<p> <?php echo $post['post_text']; ?> </p>
		 		<span class='post-date'> <?php echo $post['post_date']; ?> </span>
	 		</div>
	 	</article>
	   

<?php
	  }
	}
} // end author posts


function get_author($author_id){

}

function display_posts($db, $post_count = 16){

		$sql = "SELECT * FROM `posts` ORDER BY `post_id` DESC LIMIT {$post_count} ";
		$posts = $db->query($sql);

		if(!empty($posts) ) {
	   
			foreach($posts as $post) :

					include 'post-template.php';

		  	endforeach;
		} else {
			echo "No Posts";
		}
}