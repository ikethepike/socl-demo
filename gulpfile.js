// Project Settings

var dir = ".";
	
	// FTP
var	host = 'ftp.wearepanopticon.com'
	user = 'dev@wearepanopticon.com'
	password = 'VeifhInxq[bO'
	directory = './curatable';


	// DB controls
	var dbhost 		= 'localhost'
	    dbport 		= 21 //change if required
	    dbuser 		= 'root'
	    dbpassword 	= ''
	    database 	= 'curatable'


// Date and Time

var currentdate = new Date(); 
var datetime = "Last Sync: " + currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + " @ "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();	

// Dependencies

	var gulp = require('gulp'),
	uglify = require('gulp-uglify')
	autoprefixer = require('gulp-autoprefixer')
	sass = require('gulp-sass')
	gutil = require('gulp-util')
	pngmin = require('gulp-pngmin')
	svg2png = require('gulp-svg2png')
	size = require('gulp-size')
	notify = require("gulp-notify")
	imagemin = require('gulp-imagemin')
	pngquant = require('imagemin-pngquant')
	ftp = require( 'gulp-ftp' )
	livereload = require('gulp-livereload')
	git = require('gulp-git')
	MysqlTools= require('mysql-tools')
	plumber = require('gulp-plumber');
	


//JS uglify
	gulp.task('compress', function() {
	  return gulp.src(dir + '/js/scripts.js')
	  	.pipe(plumber())
	    .pipe(uglify())
	    .pipe(plumber.stop())
	    .pipe(gulp.dest(dir + '/js/min/'))
	});

// CSS Autoprefixer
gulp.task('autoprefixer', function () {
    return gulp.src(dir + '/css/**/*.css')
        .pipe(autoprefixer({
            browsers: ['last 3 versions'],
            cascade: false
        }))
    .pipe(gulp.dest(dir + '/css/'));
});


// PNG Minification
gulp.task('pngmin', function(){
	  gulp.src(dir + '/img/**/*.png')
	  .pipe(pngmin())
      .pipe(gulp.dest(dir + '/img'));

});

// Image optimization

gulp.task('img', function () {
    return gulp.src(dir + '/img/unoptimized/*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: true}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest(dir + '/img/'));
});


//SCSS Processing
gulp.task('sass', function () {
    gulp.src('sass/**/*.scss')
		.pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(dir + '/css'));
        console.log('SASS compile done');
});

// SVG to PNG
gulp.task('svg2png', function () {
    gulp.src(dir+'/img/**/*.svg')
        .pipe(svg2png())
        .pipe(gulp.dest('./build'));
});

// Project Size
gulp.task('size', function () {
    var s = size();
    return gulp.src('fixture.js')
        .pipe(s)
        .pipe(gulp.dest('dist'))
        .pipe(notify({
            onLast: true,
            message: function () {
                return 'Total size ' + s.prettySize;
            }
        }));
});

// DB Tools

// create database dump sql file
gulp.task('dump', function(){
	var tool = new MysqlTools();
	 tool.dumpDatabase({
	      host: dbhost
	    , user: dbuser
	    , password: dbpassword
	    , dumpPath: './'
	    , database: database
	}, function (error, output, message, dumpFileName) {
	    if (error instanceof Error) {
	       console.log('failed to export database...');
	    } else {
	       console.log(output);
	       console.log(message);
	       console.log(dumpFileName);
	    }
	});
});


// restore dump sql file to database
gulp.task('import', function(){
	var tool = new MysqlTools();
	 tool.restoreDatabase({
	      host: host+':'+dbport
	    , user: user
	    , password: password
	    , sqlFilePath: 'db.sql'
	    , database: database
	}, function (error, output, message) {
	    if (error instanceof Error) {
	       console.log(code);
	    } else {
	       console.log(output);
	       console.log(message);
	    }
	});
});




// FTP

gulp.task('ftp', function () {
	console.log('Transferring to: '+ directory + '. This may take a while...') 
    return gulp.src('*')
        .pipe(ftp({
            host: host,
            user: user,
            pass: password,
            remotePath: directory
        }))
        // you need to have some kind of stream after gulp-ftp to make sure it's flushed 
        // this can be a gulp plugin, gulp.dest, or any kind of stream 
        // here we use a passthrough stream 
        .pipe(gutil.noop());
});

// Git 

gulp.task('add', function(){
  return gulp.src('.')
    .pipe(git.add());
});

gulp.task('commit', function(){
  return gulp.src('*')
    .pipe(git.commit(['build'+ datetime, 'Automatic build compilation']));
});

gulp.task('push', function(){
  git.push('origin', 'master', function (err) {
    if (err) throw err;
  });
});

gulp.task('pull', function(){
  git.pull('origin', 'master', {args: '--rebase'}, function (err) {
    if (err) throw err;
  });
});


// Watch Task
gulp.task('watch', function(){
	gulp.watch(dir+'/img/unoptimized/*', ['img']);
	gulp.watch(dir+'/js/*.js', ['compress']);
	gulp.watch(dir+'/sass/**/*.scss', ['sass']);
	gulp.watch(dir+'/css/**/*.css', ['autoprefixer']);
});

gulp.task('live', function(){
	livereload.listen();
	gulp.watch(dir+'/sass/**/*.scss', ['sass']);
	gulp.watch(dir+'/css/**/*.css', ['autoprefixer']);
});


gulp.task('build', function(){
	gulp.start('compress', 'sass', 'autoprefixer', 'img', 'add', 'push', 'ftp');

});

gulp.task('default', function() {
    gulp.start('watch');
});