</head>

<body class="<?php if(!logged_in()){ echo 'overflow'; } ?>">

<header id='site-header'>
<div id='logo-wrapper'>
	<nav>
		<a href='<?php echo BASE_URL; ?>' title='home'>
			<h1> Socl. </h1>
		</a>
	</nav>
</div>
<?php if(logged_in() || !empty($_SESSION['username'])){ ?>
	<div id='header-background'></div>
<?php } ?>
</header>