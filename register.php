<?php 

require "core.php";

if(!empty($_POST)){

	global $db; 

	$username 	= $_POST['username'];
	$password 	= $_POST['password'];
	$fname 		= $_POST['first_name'];
	$lname 		= $_POST['last_name'];
	$email 		= $_POST['email'];


	$options = array(
		"cost" => 11
	);

	$password = password_hash($password, PASSWORD_BCRYPT, $options );

	$sql="INSERT INTO users (username, password, first_name, last_name, email) VALUES ('{$username} ', '{$password}', '{$fname}', '{$lname}', '{$email}')";
	
	$stmt= $db->prepare($sql);
	
	if($stmt->execute() == false){
		echo "<div class='message error'> Error creating user. </div>";
	} else {
		echo "<div class='message success'> User created successfully. </div>";
	}

}

include "inc/header.php"; ?>

<title> Socl | Register </title>

<?php include 'inc/toolbar.php'; ?>



	<div id='register-pane'>
		<header>
			<h1> Welcome to Socl </h1>
			<span>
				Let's get you all set up using the world's most popular HATII based social network. 
			</span>
		</header>


		<form id='register-form' method="POST">

			<div class='register-field'>
				<label for="username"> Username </label>
				<input name='username' type='text' required placeholder='Username' /> 
			</div>

			<div class='register-field'>
				<label for="Password"> Password </label>
				<input name='password' type='password' required placeholder='Password' /> 
			</div>

			<div class='register-field'>
				<label for="Name"> First Name </label>
				<input type='text' name='first_name' required placeholder='First name' /> 
			</div>

			<div class='register-field'>
				<label for="Name"> Last Name </label>
				<input type='text' name='last_name' required placeholder='Last name' /> 
			</div>

			<div class='register-field'>
				<label for="Email"> Email </label>
				<input type='email' name='email' required placeholder='Email' /> 
			</div>

			<div id='submit-wrapper'>
				<input type='submit' value='Register'>
			</div>
			
		</form>
	</div>

</body>
</html>